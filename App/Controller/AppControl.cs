﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestEFSOL_Console.App.Model;
using TestEFSOL_Console.App.View;


namespace TestEFSOL_Console.App.Controller
{
    class AppControl
    {
        Addition addition = new Addition();
        Subtraction subtraction = new Subtraction();
        Multiplication multiplication = new Multiplication();
        Division division = new Division();
        ViewConsole viewConsole = new ViewConsole();
        ViewForm1 form1 = new ViewForm1();
        public void AppRun()
        {
            /*
            Console.Write("Use graphics(YES input:1):");
            if (Console.ReadLine() == "1")
            {
                form1.ShowDialog();
            }
            else */
            while (true)
            {
                Console.Clear();
                viewConsole.AppInfo();
                SwitchCalc(Console.ReadLine());
                Console.ReadKey();
            }
            
            
        }
        private void SwitchCalc(string value)
        {
            char chr = '0';
            if (value.IndexOf('+') > 0) chr = '+';
            if (value.IndexOf('-') > 0) chr = '-';
            if (value.IndexOf('*') > 0) chr = '*';
            if (value.IndexOf('/') > 0) chr = '/';

            string[] num = value.Split(chr);
            
            switch (chr)
            {
                case '+':
                    addition.Execute(double.Parse(num[0]), double.Parse(num[1]));
                    Console.WriteLine(addition.Info());
                    break;
                case '-':
                    subtraction.Execute(double.Parse(num[0]), double.Parse(num[1]));
                    Console.WriteLine(subtraction.Info());
                    break;
                case '*':
                    multiplication.Execute(double.Parse(num[0]), double.Parse(num[1]));
                    Console.WriteLine(multiplication.Info());
                    break;
                case '/':
                    division.Execute(double.Parse(num[0]), double.Parse(num[1]));
                    foreach (string s in division.InfoArray())
                    {
                        Console.WriteLine(s);
                    };
                    break;
            }
        }
    }
}
