﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestEFSOL_Console.App.Model;
using TestEFSOL_Console.App.Controller;

/// <summary>
/// Developer : Abdynazarov Kamchybek, email:qweyn.qwe@gmail.com
/// </summary>
namespace TestEFSOL_Console.App
{
    class Program
    {
        static void Main(string[] args)
        {
            AppControl appControl = new AppControl();
            appControl.AppRun();
        }
    }
}
