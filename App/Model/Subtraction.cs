﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEFSOL_Console.App.Model
{
    class Subtraction
    {
        private double a, b, answer;
        public void Execute(double a, double b)
        {
            this.a = a;
            this.b = b;
            answer = a - b;
        }
        public string Info()
        {
            return a + " - " + b + " = " + answer;
        }
    }
}
