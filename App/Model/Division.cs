﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEFSOL_Console.App.Model
{
    /// 
    /// Column Division      --&--     manual division
    /// 
    class Division
    {
        private double a, b, a1;
        string answer = "";
        private ArrayList infoArray = new ArrayList();
        public void Execute(double a, double b)
        {
            this.a = a;
            this.a1 = a;
            this.b = b;
            /////////////////////////////////////////////////| 22,67 / 4,5 = 2267 / 450 = 5,0377777777 |
            string [] num1 = a.ToString().Split(',');
            string [] num2 = b.ToString().Split(',');

            int fraction1 = 0, fraction2 = 0;

            if (num1.Length > 1) fraction1 = num1[1].Length;
            if (num2.Length > 1) fraction2 = num2[1].Length;

            int fractionMax = Math.Max(fraction1, fraction2);

            if (fractionMax > 0)
            {
                int toInt = int.Parse(Math.Pow(10, fractionMax).ToString());
                a *= toInt;                                                    // 2267
                a1 *= toInt;                                                   // 2267
                b *= toInt;                                                   // 450
            }
            /////////////////////////////////////////////////////////////////
            bool comma = false;
            int forI = 0;
            string spaceChar = "";
            infoArray.Add(spaceChar + a + "|" + b);
            infoArray.Add("");
            while (a !=0)
            {
                if (a >= b)
                {
                    int ans = DivNum(int.Parse(a.ToString()), int.Parse(b.ToString()));
                    a -= (b * ans);
                    answer += ans.ToString();
                    infoArray.Add(spaceChar + b * ans+"             " + b+" * "+ans + " = "+ b * ans);
                    spaceChar +=new String(' ', (b* ans).ToString().Length-a.ToString().Length);
                    infoArray.Add(spaceChar + a+ "             " + ((b * ans)+a)+" - "+ b * ans+" = " + a);
                }
                else answer += "0";                                     //5,0

                if (comma == false)
                {
                    if (answer.Length > 0) answer += ",";               //5,
                    else answer = "0,";
                    comma = true;
                }

                if (comma == true) forI++;
                if (forI > 10) break;
                a *= 10;
                
            }
            infoArray[1] = new String(' ', a1.ToString().Length) +"|"+ double.Parse(answer);        
        }
        private int DivNum(int a, int b)
        {
            int i = 0;
            while (a >= b)
            {
                a -= b;
                i++;
            }

            return i;
        }
        public ArrayList InfoArray()
        {
            return infoArray;
        }
    }
}
