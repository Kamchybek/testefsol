﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEFSOL_Console.App.View
{
    class ViewConsole
    {
        public void AppInfo()
        {
            string text = "-----EFSOL-Test-Console-App-----\n" +
                "--Example: Addition        12+12\n" +
                "--Example: Subtraction     24-12\n" +
                "--Example: Multiplication  12*2\n" +
                "--Example: Division        24/2 & 22,67/4,5\n" +
                "----------------------------\n"+
                "Input:";
            Console.WriteLine(text);
        }
    }
}
